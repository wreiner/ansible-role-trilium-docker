# Ansible Role: trilium-docker

Ansible role to create trilium docker container.

## Requirements

No special requirements; note that this role requires root access, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:

    - hosts: container-workers
      roles:
        - role: wreiner.trilium-docker
          become: yes

## Dependencies

This role depends on installed and configured docker container runtime.

## Role Variables

The version must be specified with:

```
trilium_docker__trilium_version: 0.47.5
```

The interface to which the port should be exposed must be sepcified.

```
trilium__listen_address: "192.168.X.Y"
```

The port exposed will be 38083 on the specified interface.
